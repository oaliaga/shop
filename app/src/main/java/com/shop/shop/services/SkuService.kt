package com.shop.shop.services

import android.app.IntentService
import android.content.Intent
import android.util.Log
import com.shop.shop.carro.SkuDto
import com.shop.shop.carro.SkuOpenHelper
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception
import java.net.URL

class SkuService  : IntentService("SkuService")  {



    override fun onHandleIntent(intent: Intent?) {
        Log.i("SHOP", "SkuService started")



        try {
            var skuDbHelper = SkuOpenHelper(this)

            val result = URL("http://4p1.cl/private/iot/products").readText()

            var arrayJson=JSONArray(result)

            Log.i("SHOP", "SkuService arrayJson:"+arrayJson.length())

            if(arrayJson.length()>0){
                skuDbHelper.deleteAllSku()
            }

            for (i in 0..(arrayJson.length() - 1) ) {
                val item = arrayJson.getJSONObject(i)
                val estado=item.getString("estado")
                val sku=item.getString("id")
                val nombre=item.getString("nombre")
                var precio=item.getString("precio")
                val stock=item.getInt("stock")

                var image:String=""
                var maker:String=""
                var ingredients:String=""
                var nutrition:String=""
                var description:String=""



                try {  image=item.getString("imagen")}catch (e: Exception){}
                try {  maker=item.getString("fabricante")}catch (e: Exception){}
                try {  ingredients=item.getString("ingredientes")}catch (e: Exception){}
                try {  nutrition=item.getString("nutricion")}catch (e: Exception){}
                try { description=item.getString("descripcion")}catch (e: Exception){}


                Log.i("SHOP","SkuService nombre{"+i+"}:"+nombre+" stock:"+stock+" desc:"+description+" image:"+image)

                precio=precio.replace("$","")
                precio=precio.replace(".","")
                var sku1= SkuDto()
                sku1.sku=sku
                sku1.name=nombre
                sku1.price=precio.toInt()
                sku1.stock=stock

                sku1.description=description
                sku1.nutrition=nutrition
                sku1.ingredients=ingredients
                sku1.maker=maker

                sku1.image=image





                try {
                    skuDbHelper.insertSku(sku1)
                }catch (e: Exception){
                    skuDbHelper.updateSku(sku1)
                }

            }

        }catch (exception:Exception){
            Log.e("SHOP",exception.toString())
        }
    }
}