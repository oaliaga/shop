package com.shop.shop.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.shop.shop.R

class PayActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pay)
    }


    override fun onBackPressed() {

        var intent: Intent? = null
        intent = Intent(applicationContext, SearchActivity::class.java)
        startActivity(intent)
    }
}