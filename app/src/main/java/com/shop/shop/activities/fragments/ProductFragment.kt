package com.shop.shop.activities.fragments

import android.content.Context
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.LayoutInflater
import android.view.SurfaceView
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.shop.shop.R
import com.shop.shop.carro.CarroDto
import com.shop.shop.carro.CarroOpenHelper
import com.shop.shop.carro.SkuDto
import com.shop.shop.carro.SkuOpenHelper
import com.squareup.picasso.Picasso
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

class ProductFragment: Fragment(){


    private var mListener: OnFragmentInteractionListener? = null

    var skuDto:SkuDto?=null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val rootView = inflater.inflate(R.layout.fragment_product, container, false)

        val args = arguments
        var sku_=args!!.getString("sku")
        Log.i("SHOP","sku___:"+sku_+"_")

        var skuDbHelper = SkuOpenHelper(context!!)
        skuDto= skuDbHelper.getSku(sku_)

        var tabs_main=rootView.findViewById<TabLayout>(R.id.tabs_main)
        var viewpager_main=rootView.findViewById<ViewPager>(R.id.viewpager_main)

        tabs_main.setTabTextColors(R.color.colorTab,R.color.colorTabSelect)

        val fragmentAdapter = MyPagerAdapter(activity!!.supportFragmentManager)
        fragmentAdapter.setSku(sku_)

        viewpager_main.adapter = fragmentAdapter

        tabs_main.setupWithViewPager(viewpager_main)

        var imageView= rootView.findViewById<ImageView>(R.id.imageView)

        if(skuDto!!.image!=null&&skuDto!!.image!="") {
            Picasso.with(context).load(skuDto!!.image).into(imageView);
        }

        var caja_agregar=rootView.findViewById<ConstraintLayout>(R.id.caja_agregar)
        caja_agregar.setOnClickListener(View.OnClickListener {





            var carroDbHelper = CarroOpenHelper(context!!)

            var carroDto=CarroDto()
            carroDto.sku=skuDto!!.sku
            carroDto.name=skuDto!!.name
            carroDto.price=skuDto!!.price
            carroDto.quantity=1
            carroDto.image=skuDto!!.image
            try{
                carroDbHelper.insertCarro(carroDto)
            }catch (e: Exception){
                carroDbHelper.updateCarro(carroDto)
            }
            mListener!!.agregar(sku_)
        })




        var text_name=rootView.findViewById<TextView>(R.id.text_name)


        var text_precio=rootView.findViewById<EditText>(R.id.text_precio)

        var text_stock=rootView.findViewById<EditText>(R.id.text_stock)







        if(skuDto!=null) {
            text_name.text = skuDto!!.name
            text_precio.setText(""+format(skuDto!!.price))
            text_stock.setText(""+skuDto!!.stock)
        }
        return rootView
    }


    fun format(valor:Int):String{

        val df = DecimalFormat.getCurrencyInstance() as DecimalFormat
        val dfs = DecimalFormatSymbols()
        dfs.setCurrencySymbol("")
        dfs.setMonetaryDecimalSeparator(',')
        dfs.setGroupingSeparator('.')
        df.setDecimalFormatSymbols(dfs)
        val k = df.format(valor)
        return k
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    interface OnFragmentInteractionListener {


        fun agregar(codigo:String)
    }
}