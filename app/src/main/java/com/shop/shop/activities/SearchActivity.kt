package com.shop.shop.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.*
import com.shop.shop.R
import com.shop.shop.activities.fragments.MasFragment
import com.shop.shop.activities.fragments.ProductFragment
import com.shop.shop.activities.fragments.QrFragment
import com.shop.shop.autocomplete.Customer
import com.shop.shop.autocomplete.CustomerAdapter
import com.shop.shop.carro.SkuOpenHelper


class SearchActivity : AppCompatActivity() , MasFragment.OnFragmentInteractionListener, QrFragment.OnFragmentInteractionListener,ProductFragment.OnFragmentInteractionListener {

    var customers: ArrayList<Customer>? = null
    var skuSt:String?=null
    var tag="ONE"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)



        val root_layout = findViewById<LinearLayout>(R.id.root_layout)

        if (!existsFragmentByTag(tag)) {
            addFragment()
        }

        val buttonBuscar = findViewById<ImageView>(R.id.buttonBuscar)

        buttonBuscar.setOnClickListener(View.OnClickListener {
            if(skuSt!=null)
             productFragment(skuSt!!)
        })

        var auto_complete_text_view= findViewById<AutoCompleteTextView>(R.id.textBuscar)


        var skuDbHelper = SkuOpenHelper(this)

       var skus= skuDbHelper.getAllSku()
       var size= skus.size

        val array = arrayOfNulls<String>(size)

        for (i in array.indices) {
            array[i] = skus[i].sku+", "+ skus[i].name

        }


        // Initialize a new array adapter object
        val adapter = ArrayAdapter<String>(
            this, // Context
            android.R.layout.simple_dropdown_item_1line, // Layout
            array // Array
        )


        // Set the AutoCompleteTextView adapter
        auto_complete_text_view.setAdapter(adapter)


        // Auto complete threshold
        // The minimum number of characters to type to show the drop down
        auto_complete_text_view.threshold = 1


        // Set an item click listener for auto complete text view
        auto_complete_text_view.onItemClickListener = AdapterView.OnItemClickListener{
                parent,view,position,id->
            val selectedItem = parent.getItemAtPosition(position).toString()
           val position=selectedItem.indexOf(",")

            skuSt=selectedItem.substring(0,position)


            Log.i("SHOP","Selected :$selectedItem ____" + skuSt)
        }


        // Set a dismiss listener for auto complete text view
        auto_complete_text_view.setOnDismissListener {
            //Toast.makeText(applicationContext,"Suggestion closed.",Toast.LENGTH_SHORT).show()
        }


        // Set a click listener for root layout
        root_layout.setOnClickListener{
            val text = auto_complete_text_view.text
            //Toast.makeText(applicationContext,"Inputted : $text",Toast.LENGTH_SHORT).show()
        }


        // Set a focus change listener for auto complete text view
        auto_complete_text_view.onFocusChangeListener = View.OnFocusChangeListener{
                view, b ->
            if(b){
               //auto_complete_text_view.showDropDown()
            }
        }

    }



    override fun onBackPressed() {

        addFragment2()
    }

    private fun addFragment() {
        val fragment = MasFragment()
        val ft = supportFragmentManager.beginTransaction()

        ft.add(R.id.ReplaceFrame, fragment, tag)
        if (!supportFragmentManager.isStateSaved) {
            ft.commit()
        }

    }
    private fun addFragment2() {
        val fragment = MasFragment()
        val ft = supportFragmentManager.beginTransaction()

        ft.replace(R.id.ReplaceFrame, fragment, tag)
        if (!supportFragmentManager.isStateSaved) {
            ft.commit()
        }

    }
    private fun qrFragment() {
        val fragment = QrFragment()
        val ft = supportFragmentManager.beginTransaction()

        ft.replace(R.id.ReplaceFrame, fragment, tag)
        if (!supportFragmentManager.isStateSaved) {
            ft.commit()
        }

    }

    private fun productFragment(sku_:String) {
        val fragment = ProductFragment()
        val args = Bundle()
        args.putString("sku", sku_)
        fragment.setArguments(args)

        val ft = supportFragmentManager.beginTransaction()

        ft.replace(R.id.ReplaceFrame, fragment, tag)
        if (!supportFragmentManager.isStateSaved) {
            ft.commit()
        }

    }

    fun AppCompatActivity.existsFragmentByTag(tag: String): Boolean {
        return supportFragmentManager.findFragmentByTag(tag) != null
    }

    override fun cambiaQr(){

        qrFragment()

    }

    override fun product(codigo:String){

        productFragment(codigo)

    }

    override fun agregar(codigo:String){

        var intent: Intent? = null
        intent = Intent(applicationContext, ProductListActivity::class.java)
        startActivity(intent)

    }

}