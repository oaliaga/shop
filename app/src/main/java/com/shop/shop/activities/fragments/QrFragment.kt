package com.shop.shop.activities.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.Toast
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import com.shop.shop.R


class QrFragment: Fragment(), SurfaceHolder.Callback, Detector.Processor<Barcode>{

    private var mListener: OnFragmentInteractionListener? = null

    lateinit var cameraSource : CameraSource
    var surfaceHolder : SurfaceHolder? = null
    lateinit var cameraView : SurfaceView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val rootView = inflater.inflate(R.layout.fragment_qr, container, false)
        cameraView = rootView.findViewById<SurfaceView>(R.id.camera_view)
        cameraView.setZOrderMediaOverlay(true)

        surfaceHolder = cameraView.holder

        val barcodeDetector = BarcodeDetector.Builder(activity)
            .setBarcodeFormats(Barcode.DATA_MATRIX or Barcode.QR_CODE)
            .build()

        if (!barcodeDetector.isOperational) {
            Toast.makeText(activity, "Could not set up the detector!", Toast.LENGTH_SHORT).show()

        } else {
            cameraSource = CameraSource.Builder(context, barcodeDetector)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedFps(24F)
                .setAutoFocusEnabled(true)
                .setRequestedPreviewSize(1920, 1024)
                .build()

            cameraView.holder.addCallback(this)
        }

        barcodeDetector.setProcessor(this)

        return rootView
    }

    override fun surfaceChanged(p0: SurfaceHolder?, p1: Int, p2: Int, p3: Int) {
    }

    override fun surfaceDestroyed(p0: SurfaceHolder?) {
        cameraSource.stop()
    }

    @SuppressLint("MissingPermission")
    override fun surfaceCreated(p0: SurfaceHolder?) {
        cameraSource.start(cameraView.holder)
    }

    override fun release() {
    }

    override fun receiveDetections(detections : Detector.Detections<Barcode>?) {

        val barcodes = detections!!.detectedItems

        if(barcodes!=null&&barcodes.size()>0){

            val display=barcodes.valueAt(0).displayValue

            if (display!=null&&display.length>3) {
                Log.i("SHOP","receiveDetections:"+display)
               // Toast.makeText(activity,display,Toast.LENGTH_SHORT).show()
                var position = display.indexOf("sku=")
                if (position >= 0) {
                    var sku = display.substring(position + 4)
                    Log.i("SHOP", "sku:" + sku)
                    mListener!!.product(sku)
                }
                Log.i("SHOP", display)
            }
        }


    }



    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    interface OnFragmentInteractionListener {


        fun product(codigo:String)
    }

}