package com.shop.shop.activities.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.shop.shop.R
import com.shop.shop.carro.SkuOpenHelper

class ThirdFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        var sku = arguments!!.getString("sku");
        Log.i("SHOP","sku 3:"+sku)



        var skuDbHelper = SkuOpenHelper(context!!)
        var skuDto= skuDbHelper.getSku(sku)

        var view= inflater!!.inflate(R.layout.fragment_third, container, false)
        var text=view.findViewById<TextView>(R.id.text)
        var desc=skuDto!!.ingredients
        Log.i("SHOP","sku 3 ingredients:"+desc)
        text.setText(desc)

        return view


    }
}