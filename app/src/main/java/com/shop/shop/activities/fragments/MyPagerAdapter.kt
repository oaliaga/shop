package com.shop.shop.activities.fragments

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.shop.shop.carro.SkuDto
import android.os.Bundle



class MyPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    var sku_:String?=null

    fun setSku(sku:String){
        sku_=sku
    }
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                var f1=FirstFragment()
                val args1 = Bundle()
                args1.putString("sku", sku_)
                f1.arguments = args1
                return f1
            }
            1 -> {
                var f2=SecondFragment()
                val args2 = Bundle()
                args2.putString("sku", sku_)
                f2.arguments = args2
                return f2
            }
            2 -> {
                var f3=ThirdFragment()
                val args3 = Bundle()
                args3.putString("sku", sku_)
                f3.arguments = args3
                return f3
            }
            else -> {
                var f4=FourthFragment()
                val args4 = Bundle()
                args4.putString("sku", sku_)
                f4.arguments = args4
                return f4
            }
        }
    }

    override fun getCount(): Int {
        return 4
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Descripción"
            1 -> "Nutrición"
            2 -> "Ingredientes"
            else -> {
                return "Fabricante"
            }
        }
    }

}