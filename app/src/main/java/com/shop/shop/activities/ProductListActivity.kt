package com.shop.shop.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.LinearLayout
import com.shop.shop.R
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.shop.shop.carro.CarroOpenHelper
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_product_list.*


class ProductListActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_list)

        val listado = findViewById<LinearLayout>(R.id.listado)


        val layoutInflater:LayoutInflater = LayoutInflater.from(applicationContext)

        var carroDbHelper = CarroOpenHelper(this)
        var carro=carroDbHelper.getAllCarro()
        Log.i("SHOP","sizE:"+carro.size)

        for (i in 0..(carro.size - 1) ) {
            var view: View = layoutInflater.inflate(
                R.layout.fragment_linea, // Custom view/ layout
                root_layout, // Root layout to attach the view
                false // Attach with root layout or not
            )
            var carroDto=carro.get(i)
            var text_linea1=view.findViewById<TextView>(R.id.text_linea1)
            var text_linea2=view.findViewById<TextView>(R.id.text_linea2)
            try{text_linea1.setText(carroDto.name)}catch (e: Exception){}
            try{text_linea2.setText(""+carroDto.quantity)}catch (e: Exception){}


            var imageView= view.findViewById<ImageView>(R.id.image_linea)
            Log.i("SHOP","carroDto "+i+" .image:"+carroDto!!.image)
            if(carroDto!!.image!=null&&carroDto!!.image!="") {
                Picasso.with(applicationContext).load(carroDto!!.image).into(imageView);
            }


            listado.addView(view)
        }

        val vaciar = findViewById<Button>(R.id.vaciar)
        vaciar.setOnClickListener(View.OnClickListener {
            carroDbHelper.deleteAllCarro()
            var intent: Intent? = null
            intent = Intent(applicationContext, SearchActivity::class.java)
            startActivity(intent)
        })

        val comprar = findViewById<Button>(R.id.comprar)
        comprar.setOnClickListener(View.OnClickListener {
            var intent: Intent? = null
            intent = Intent(applicationContext, PayActivity::class.java)
            startActivity(intent)
        })
    }


    override fun onBackPressed() {

        var intent: Intent? = null
        intent = Intent(applicationContext, SearchActivity::class.java)
        startActivity(intent)
    }
}