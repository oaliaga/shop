package com.shop.shop.activities

import android.content.Intent
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.ImageView
import android.widget.TextView
import com.shop.shop.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_main)


        val rotate = RotateAnimation(0f, 1360f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        rotate.duration = 1500
        rotate.interpolator = LinearInterpolator()

        val myImg = findViewById<ImageView>(R.id.rueda)


        myImg.startAnimation(rotate);

    }


    override fun onResume() {
        super.onResume()


        EsperaTask().execute()
    }

    inner class EsperaTask: AsyncTask<String, String, String>() {


        override fun onPreExecute() {
            super.onPreExecute()

        }
        override fun doInBackground(vararg arr: String?): String {

            Thread.sleep(2000)
            var intent: Intent? = null
            intent = Intent(applicationContext, RegisterActivity::class.java)
            startActivity(intent)
            return ""
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

        }
    }

}
