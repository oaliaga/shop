package com.shop.shop

import android.app.Application
import android.content.Intent
import com.shop.shop.carro.CarroOpenHelper
import com.shop.shop.carro.SkuDto
import com.shop.shop.carro.SkuOpenHelper
import com.shop.shop.services.SkuService

class ShopApp : Application(){

    override fun onCreate() {
        super.onCreate()

        var carroDbHelper = CarroOpenHelper(this)
        carroDbHelper.getWritableDatabase()

        var skuDbHelper = SkuOpenHelper(this)
        skuDbHelper.getWritableDatabase()




        val intent = Intent(this, SkuService::class.java)
        startService(intent)
    }



}