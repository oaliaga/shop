package com.shop.shop.carro

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log

class CarroOpenHelper (context: Context) : SQLiteOpenHelper(context, "Carro.db", null, 3){


    val TABLE_NAME = "carro"


    val DATABASE_CREATE = "CREATE TABLE if not exists " + TABLE_NAME + " (" +
            COLUMN_SKU + " TEXT PRIMARY KEY, " +
            COLUMN_NAME + " TEXT NULL," +
            COLUMN_IMG + " IMG NULL," +
            COLUMN_PRICE + " REAL NULL," +
            COLUMN_QUANTITY + " INTEGER NULL" +
            ")"


    companion object {





        val COLUMN_NAME = "_name"
        val COLUMN_QUANTITY = "_quantity"
        val COLUMN_PRICE = "_price"
        val COLUMN_SKU = "_sku"

        val COLUMN_IMG = "_img"

    }

    override fun onCreate(db: SQLiteDatabase) {
        try {
            Log.i("SHOP","DATABASE_CREATE:"+DATABASE_CREATE)
            db.execSQL(DATABASE_CREATE)
        } catch (e: Exception) {
            Log.e("SHOP","Error creating table: " + e.message, e)
        }

    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        try {
            db.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        } catch (e: Exception) {
            Log.e("SHOP","Erase error table: " + e.message, e)
        }

        onCreate(db)
    }

    fun insertCarro(carro: CarroDto) {
        val database = this.writableDatabase
        val values = ContentValues()

        values.put(COLUMN_NAME, carro.name)
        values.put(COLUMN_SKU, carro.sku)
        values.put(COLUMN_PRICE, carro.price)
        values.put(COLUMN_IMG, carro.image)
        values.put(COLUMN_QUANTITY, carro.quantity)
        Log.i("SHOP","___db insert:" + carro.toString())
        database.insert(TABLE_NAME, null, values)
        database.close()
    }

    fun updateCarro( carro: CarroDto) {
        val database = this.writableDatabase
        val values = ContentValues()
        values.put(COLUMN_NAME, carro.name)
        values.put(COLUMN_IMG, carro.image)
        values.put(COLUMN_PRICE, carro.price)
        values.put(COLUMN_QUANTITY, carro.quantity)
        val selection = "$COLUMN_SKU = ?"
        val selectionArgs = arrayOf(carro.sku)
        Log.i("SHOP","___db update:" + carro.toString());
        database.update(TABLE_NAME, values, selection, selectionArgs)
        database.close()
    }




    fun deleteCarro(sku: String) {
        val database = this.writableDatabase
        val deleteQuery = "DELETE FROM  $TABLE_NAME where $COLUMN_SKU='$sku'"
        Log.i("SHOP","query" + deleteQuery);
        database.execSQL(deleteQuery)
        database.close()
    }

    fun deleteAllCarro() {
        val database = this.writableDatabase
        val deleteQuery = "DELETE FROM  $TABLE_NAME"
        Log.i("SHOP","query" + deleteQuery);
        database.execSQL(deleteQuery)
        database.close()
    }

    fun getAllCarro(): MutableList<CarroDto> {
        var cursor: Cursor? = null
        val list: MutableList<CarroDto> = mutableListOf<CarroDto>()
        try {
            val selectQuery = "SELECT  * FROM $TABLE_NAME"
            val database = this.readableDatabase
            cursor = database.rawQuery(selectQuery, null)
            if (cursor!!.moveToFirst()) {
                do {
                    val carro = CarroDto()
                    carro.sku = cursor.getString(0)
                    carro.name = cursor.getString(1)
                    try{carro.image = cursor.getString(2)} catch (e: Exception) {}
                    carro.price = cursor.getInt(3)

                    try{carro.quantity = cursor.getInt(4)} catch (e: Exception) {}
                    list.add(carro)
                } while (cursor.moveToNext())
            }
        } catch (e: Exception) {
            Log.e("SHOP","error:" + e.message, e)
        } finally {
            if (cursor != null) {
                try {
                    cursor.close()
                } catch (e1: Exception) {
                }

            }
        }
        return list
    }

    fun getCarro(sku: String): CarroDto? {
        var cursor: Cursor? = null
        var carro: CarroDto? = null
        try {
            val database = this.readableDatabase
            val selectQuery = "SELECT * FROM $TABLE_NAME where $COLUMN_SKU='$sku'"
            cursor = database.rawQuery(selectQuery, null)
            if (cursor!!.moveToFirst()) {
                do {
                    carro = CarroDto()
                    carro.sku = cursor.getString(0)
                    carro.name = cursor.getString(1)
                    try{carro.image = cursor.getString(2)} catch (e: Exception) {}
                    carro.price = cursor.getInt(3)

                    try{carro.quantity = cursor.getInt(4)} catch (e: Exception) {}






                } while (cursor.moveToNext())
            }
        } catch (e: Exception) {
            Log.e("SHOP","error:" + e.message, e)
        } finally {
            if (cursor != null) {
                try {
                    cursor.close()
                } catch (e1: Exception) {
                }

            }
        }
        return carro
    }

}