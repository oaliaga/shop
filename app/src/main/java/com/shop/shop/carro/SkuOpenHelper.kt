package com.shop.shop.carro

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log

class SkuOpenHelper (context: Context) : SQLiteOpenHelper(context, "Sku.db", null, 3){


    val TABLE_NAME = "sku"


    val DATABASE_CREATE = "CREATE TABLE if not exists " + TABLE_NAME + " (" +
            COLUMN_SKU + " TEXT PRIMARY KEY, " +
            COLUMN_NAME + " TEXT NULL," +
            COLUMN_PRICE + " REAL NULL," +
            COLUMN_STOCK + " REAL NULL," +
            COLUMN_IMG + " IMG NULL," +
            COLUMN_DESCRIPTION + " TEXT NULL," +
            COLUMN_NUTRITION + " TEXT NULL," +
            COLUMN_INGREDIENTS + " TEXT NULL," +
            COLUMN_MAKER + " TEXT NULL" +




            ")"


    companion object {




        val COLUMN_SKU = "_sku"
        val COLUMN_NAME = "_name"
        val COLUMN_PRICE = "_price"
        val COLUMN_STOCK = "_stock"
        val COLUMN_IMG = "_img"
        val COLUMN_DESCRIPTION= "_description"
        val COLUMN_NUTRITION = "_nutrition"
        val COLUMN_INGREDIENTS = "_ingredients"
        val COLUMN_MAKER = "_maker"




    }

    override fun onCreate(db: SQLiteDatabase) {
        try {
            Log.i("SHOP","DATABASE_CREATE:"+DATABASE_CREATE)
            db.execSQL(DATABASE_CREATE)
        } catch (e: Exception) {
            Log.e("SHOP","Error creating table: " + e.message, e)
        }

    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        try {
            db.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        } catch (e: Exception) {
            Log.e("SHOP","Erase error table: " + e.message, e)
        }

        onCreate(db)
    }

    fun insertSku(sku: SkuDto) {
        val database = this.writableDatabase
        val values = ContentValues()

        values.put(COLUMN_NAME, sku.name)
        values.put(COLUMN_SKU, sku.sku)
        values.put(COLUMN_PRICE, sku.price)
        values.put(COLUMN_STOCK, sku.stock)
        values.put(COLUMN_DESCRIPTION, sku.description)
        values.put(COLUMN_NUTRITION, sku.nutrition)
        values.put(COLUMN_INGREDIENTS, sku.ingredients)
        values.put(COLUMN_MAKER, sku.maker)

        values.put(COLUMN_IMG, sku.image)
        Log.i("SHOP","___db insert:" + sku.toString())
        database.insert(TABLE_NAME, null, values)
        database.close()
    }

    fun updateSku(sku: SkuDto) {
        val database = this.writableDatabase
        val values = ContentValues()
        values.put(COLUMN_NAME, sku.name)
        values.put(COLUMN_IMG, sku.image)
        values.put(COLUMN_PRICE, sku.price)
        values.put(COLUMN_STOCK, sku.stock)
        values.put(COLUMN_DESCRIPTION, sku.description)
        values.put(COLUMN_NUTRITION, sku.nutrition)
        values.put(COLUMN_INGREDIENTS, sku.ingredients)
        values.put(COLUMN_MAKER, sku.maker)

        val selection = "$COLUMN_SKU = ?"
        val selectionArgs = arrayOf(sku.sku)
        Log.i("SHOP","___db update:" + sku.toString());
        database.update(TABLE_NAME, values, selection, selectionArgs)
        database.close()
    }




    fun deleteSku(sku: String) {
        val database = this.writableDatabase
        val deleteQuery = "DELETE FROM  $TABLE_NAME where $COLUMN_SKU='$sku'"
        Log.i("SHOP","query" + deleteQuery);
        database.execSQL(deleteQuery)
        database.close()
    }

    fun deleteAllSku() {
        val database = this.writableDatabase
        val deleteQuery = "DELETE FROM  $TABLE_NAME"
        Log.i("SHOP","query" + deleteQuery);
        database.execSQL(deleteQuery)
        database.close()
    }





    fun getAllSku(): MutableList<SkuDto> {
        var cursor: Cursor? = null
        val list: MutableList<SkuDto> = mutableListOf<SkuDto>()
        try {
            val selectQuery = "SELECT  * FROM $TABLE_NAME"
            val database = this.readableDatabase
            cursor = database.rawQuery(selectQuery, null)
            if (cursor!!.moveToFirst()) {
                do {
                    val sku = SkuDto()
                    sku.sku = cursor.getString(0)
                    sku.name = cursor.getString(1)
                    sku.price = cursor.getInt(2)
                    sku.stock = cursor.getInt(3)
                    sku.image = cursor.getString(4)
                    sku.description = cursor.getString(5)
                    sku.nutrition = cursor.getString(6)
                    sku.ingredients = cursor.getString(7)
                    sku.maker = cursor.getString(8)

                    list.add(sku)
                } while (cursor.moveToNext())
            }
        } catch (e: Exception) {
            Log.e("SHOP","error:" + e.message, e)
        } finally {
            if (cursor != null) {
                try {
                    cursor.close()
                } catch (e1: Exception) {
                }

            }
        }
        return list
    }

    fun getSku(skuSt: String): SkuDto? {
        Log.i("SHOP","getSku("+skuSt+")")
        var cursor: Cursor? = null
        var sku: SkuDto? = null
        try {
            val database = this.readableDatabase
            val selectQuery = "SELECT * FROM "+TABLE_NAME+" where "+COLUMN_SKU+"='"+skuSt+"'"
            Log.i("SHOP","_"+selectQuery+"_")
            cursor = database.rawQuery(selectQuery, null)
            if (cursor!!.moveToFirst()) {
                do {
                    sku = SkuDto()
                    sku.sku = cursor.getString(0)
                    sku.name = cursor.getString(1)
                    sku.price = cursor.getInt(2)
                    sku.stock = cursor.getInt(3)
                    sku.image = cursor.getString(4)
                    sku.description = cursor.getString(5)
                    sku.nutrition = cursor.getString(6)
                    sku.ingredients = cursor.getString(7)
                    sku.maker = cursor.getString(8)
                } while (cursor.moveToNext())
            }
        } catch (e: Exception) {
            Log.e("SHOP","error:" + e.message, e)
        } finally {
            if (cursor != null) {
                try {
                    cursor.close()
                } catch (e1: Exception) {
                }

            }
        }
        return sku
    }

}