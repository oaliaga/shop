package com.shop.shop.carro

class CarroDto{


    var image: String=""
    var name: String=""
    var quantity: Int = 0
    var price: Int = 0
    var sku: String=""

    override fun toString(): String {
        val sb = StringBuffer()
        sb.append("carro sku:").append(sku)
        sb.append(" name:").append(name)
        sb.append(" quantity:").append(quantity)
        sb.append(" image:").append(image)
        sb.append(" price:").append(price).append("\n")
        return sb.toString()
    }
}