package com.shop.shop.carro

class SkuDto{


    var image: String=""
    var name: String=""

    var price: Int = 0
    var sku: String=""
    var stock: Int = 0
    var description: String=""
    var nutrition: String=""
    var ingredients: String=""
    var maker: String=""

    override fun toString(): String {
        val sb = StringBuffer()
        sb.append("sku:").append(sku)
        sb.append(" name:").append(name)

        sb.append(" stock:").append(stock)
        sb.append(" price:").append(price).append("\n")

        sb.append(" img:").append(image)

        sb.append(" description:").append(description)
        sb.append(" nutrition:").append(nutrition)
        sb.append(" ingredients:").append(ingredients)
        sb.append(" maker:").append(maker)
        return sb.toString()
    }
}